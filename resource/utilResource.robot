*** Settings ***
Library  SeleniumLibrary   timeout=10  #run_on_failure=CapturePageScreenShot

*** Variables ***

${BROWSER}    chrome 
${URL}    https://www.amazon.com.br/gp/help/customer/display.html?nodeId=508510&ref_=nav_cs_help
${DELAY}    0.5

*** Keywords ***

Dado que acesso a pagina do sistema
    Run Keyword If      '${BROWSER}' == 'HeadlessChrome'      Abrir navegador Chrome headless
    ...     ELSE IF     '${BROWSER}' == 'HeadlessFirefox'     Abrir navegador Firefox headless
    ...     ELSE IF     '${BROWSER}' == 'chrome'              Abrir navegador Chrome local
    ...     ELSE     Abrir navegador Chrome headless
    #Set Selenium Speed    ${DELAY}

Abrir navegador Chrome headless
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    test-type
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Call Method    ${chrome_options}    add_argument    --disable-dev-shm-usage
    Create Webdriver    Chrome    chrome_options=${chrome_options}
#    Run Keyword If      "${APP_ENV}" == "test"      Set Global Variable        ${PAGE URL}    ${TEST_PAGE URL}
#    Run Keyword If      "${APP_ENV}" == "staging"      Set Global Variable        ${PAGE URL}    ${STAGING_PAGE URL}
    Go To    ${URL}
    Set Window Size    width=${1400}    height=${1200}
    Set Selenium Speed    ${DELAY}

Abrir navegador Firefox headless
    ${firefox options}=     Evaluate    sys.modules['selenium.webdriver'].firefox.webdriver.Options()    sys, selenium.webdriver
    Call Method    ${firefox options}   add_argument    -headless
    Create Webdriver    Firefox    firefox_options=${firefox options}
    Go To    ${URL}
    Set Window Size    width=${1400}    height=${1200}
    Set Selenium Speed    ${DELAY}

Abrir navegador Chrome local
    Open Browser    url=${URL}    browser=${BROWSER}
    Set Window Size    width=${1400}    height=${1200}
    Set Selenium Speed    ${DELAY}


