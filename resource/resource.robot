*** Settings ***
Library  SeleniumLibrary   timeout=10  #run_on_failure=CapturePageScreenShot

*** Variables ***

${Atendimento_cliente}     css=#nav-xshop > a:nth-child(8)
${BTN_seus_pedidos}        css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(1) > div:nth-child(1) > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${Titulo_fazer_login}      css=#authportal-main-section > div:nth-child(2) > div > div.a-section > form > div > div > div > h1
${BTN_de_configurações}    css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(2) > div:nth-child(1) > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${BTN_Amozon_Covid_19}     css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(3) > div:nth-child(1) > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${Titulo_principal_da_tela_atendimento_cliente}    css=#a-page > div.cs-help-v4 > div.cs-help-header > div > div > div > a
${BTN_devolucao_reebolso}     css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(1) > div:nth-child(2) > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${BTN_gerencia_endereco}      css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(2) > div:nth-child(2) > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${BTN_denuncia_algo_suspeito}   css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(3) > div:nth-child(2) > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${BTN_disposivo_digital}      css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(1) > div.a-column.a-span4.a-span-last > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${BTN_disp_dig}   css=#heading > h1
${BTN_sua_conta}  css=#a-page > div:nth-child(30) > div:nth-child(6) > div > div.a-section.a-spacing-large.ss-landing-container-wide > div:nth-child(2) > div.a-column.a-span4.a-span-last > a > div > div > div > div.a-column.a-span9.ss-rich-card-column-text.a-span-last > h3
${BTN_conta}   css=#a-page > div.a-container > div > div.a-row.a-spacing-base > h1

*** Keywords ***

E clico no Atendimento ao cliente
    Wait Until Element Is Visible    locator=${Atendimento_cliente}
    Click Element    locator=${Atendimento_cliente}

Quando clico em Seus pedidos
    Wait Until Element Is Visible    locator=${BTN_seus_pedidos}
    Click Element     locator=${BTN_seus_pedidos} 

Entao o sistema deve exibir a tela de login
    Element Text Should Be    locator=${Titulo_fazer_login}    expected=Fazer login
    
Finalizar teste
    Close Browser

Quando clico em configurações de pagamento
    Wait Until Element Is Visible     locator=${BTN_de_configurações} 
    Click Element                     locator=${BTN_de_configurações} 

Quando clico em Amazon e COVID-19
    Wait Until Element Is Visible     locator=${BTN_Amozon_Covid_19}
    Click Element                     locator=${BTN_Amozon_Covid_19}
                                      
Entao o sistema exibi uma pagina de Ajuda e Serviço de atendimento ao cliente 
    Element Text Should Be     locator=${Titulo_principal_da_tela_atendimento_cliente}         expected=Ajuda e Serviço de atendimento ao cliente

Quando clico em Devoluções e reembolsos
     Wait Until Element Is Visible    locator=${BTN_devolucao_reebolso}
     Click Element                    locator=${BTN_devolucao_reebolso}

Quando clico em Gerenciar endereços
     Wait Until Element Is Visible    locator=${BTN_gerencia_endereco}
     Click Element                    locator=${BTN_gerencia_endereco} 

Quando clico em Denunciar algo suspeito
    Click Element        locator=${BTN_denuncia_algo_suspeito}

Quando clico em Suporte para dispositivos e serviços digitais
    Click Element        locator=${BTN_disposivo_digital}  

Entao o sistema exibi uma pagina de Ajuda com os serviços digitais e dispositivos
   Element Text Should Be   locator=${BTN_disp_dig}   expected=Ajuda com os serviços digitais e dispositivos

Quando clico em Sua conta
    Click Element    locator=${BTN_sua_conta} 

Entao o sistema exibi uma pagina Sua conto com mais opcoes 
   Element Text Should Be   locator=${BTN_conta}   expected=Sua conta


    

